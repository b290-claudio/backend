// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Greninja", "Lucario"],
	friends: {
		hoenn : ["May", "Max"],
		kanto : ["Brock", "Misty"]
	}
};

// Methods
trainer.talk = function(){
		return this.pokemon[0] + "! I choose you.";
	}

// Check if all properties and methods were properly added
console.log(trainer);

// Access object properties using dot notation
console.log("Result of dot notation:");
console.log(trainer.name);

// Access object properties using square bracket notation
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// Access the trainer "talk" method
console.log("Result of talk method:");
console.log(trainer.talk());

// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level){

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	let ret = [];
	this.tackle = function(target){
		target.health = target.health - this.attack;
		if(target.health <= 0){
			ret.splice(0, 2);
			ret.push(this.name + " tackled " + target.name,
			target.name + "'s health is now reduced to " + target.health,
			target.faint());
		};
		if(target.health > 0){
			ret.splice(0, 2);
			ret.push(this.name + " tackled " + target.name,
				target.name + "'s health is now reduced to " + target.health);
		};
		return ret.join("\n");
	};
	this.faint = function(){
		return this.name + " fainted.";
	};
};


// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 30);
console.log(pikachu);


// Create/instantiate a new pokemon
let rattata = new Pokemon("Rattata", 16);
console.log(rattata);


// Create/instantiate a new pokemon
let lapras = new Pokemon("Lapras", 20);
console.log(lapras);


// Invoke the tackle method and target a different object
console.log(pikachu.tackle(rattata));
console.log(rattata);

// Invoke the tackle method and target a different object
console.log(rattata.tackle(pikachu));
console.log(pikachu);


console.log(pikachu.tackle(rattata));
console.log(rattata);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}